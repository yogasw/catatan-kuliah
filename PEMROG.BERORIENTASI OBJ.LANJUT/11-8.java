/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author lik-305
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        tiga(30, 3, 1);
    }

    static void ulangwhile(double hutang, double angsuran) {
        double bayar;
        int bulan;
        bayar = 0;
        bulan = 0;
        while (bayar < hutang) {
            bayar = bayar + angsuran;
            bulan++;
            String text = String.valueOf(bayar);
            System.out.println(bulan + "membayar " + text);
        }
    }

    static void ulangfor() {
        double uang, angsuran;
        int bulan;
        uang = 0;
        bulan = 0;
        angsuran = 0;
        for (bulan = 0; uang <= 10000; bulan++) {
            uang = uang + 500;
            System.out.println(bulan + " " + uang);
        }
    }

    static void tiga(int jarak, int stop, int minum) {
        int jalan;
        int berhenti, liter;
        jalan = 0;
        berhenti = 0;
        liter = 0;
        while (jalan < jarak) {
            jalan++;
            if (jalan % 3 == 0) {
                berhenti++;
                liter=liter+minum;
            }
            if (jalan == jarak) {
                System.out.println(berhenti + " " + liter);
            }
        }
    }
}
