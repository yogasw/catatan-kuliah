/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hari.ke.pkg2;

/**
 *
 * @author lik-305
 */
class matematika {

    int c = 0;

    void pertambahan(int a, int b) {
        c = a + b;
        System.out.println(a + " + " + b + " = " + c);
    }

    void pengurangan(int a, int b) {
        c = a - b;
        System.out.println(a + " - " + b + " = " + c);
    }

    void perkalian(int a, int b) {
        c = a * b;
        System.out.println(a + " * " + b + " = " + c);
    }

    void pembagian(int a, int b) {
        c = a / b;
        System.out.println(a + " / " + b + " = " + c);
    }
}

class latihan {

    void sultan(int jumlah, double harga, double potongan) {
        /*SOAL
         Sultan membeli kain di pasar 16.
         Ia mendapatkan potongan rp.. tiap meternya.
         berapa uang yg di keluarkan sultan.
         */

        double diskon, total;
        
        diskon = jumlah * potongan;
        total = jumlah * harga - diskon;
        System.out.println("Sultan membeli kain di pasar 16");
        System.out.println("Sebanyak : " + jumlah+" Meter");
        System.out.println("Dengan Harga permeter yaitu : Rp. " + harga);
        System.out.println("jumlah potongan sebesar Rp. " + diskon);
        System.out.println("Jadi sultan hanya perlu membayar sebesar Rp. " + total);

    }
}

public class HariKeDua {

    public static void main(String[] args) {
        latihan2();
    }

    static void latihan1() {
        matematika mtk = new matematika();
        mtk.pertambahan(10, 10);
        mtk.pengurangan(10, 10);
        mtk.perkalian(10, 10);
        mtk.pembagian(10, 10);
    }

    static void latihan2() {
        latihan latihan = new latihan();
        latihan.sultan(10, 10000, 1000);
    }
}
